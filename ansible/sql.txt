---
- hosts: database
  tasks:
    - name: 1. Install MySQL
      apt: name=mysql-server state=present

    - name: 2. Move file for database
      copy:
       src: ~/virtuozas/toms/WEB-INF/classes/db.sql
       dest: ~/