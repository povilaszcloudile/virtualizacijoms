package com.povilas.account.service;

import com.povilas.account.model.User;

public interface UserService {
    void save(User user);

    User findByUsername(String username);
}
